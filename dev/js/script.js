function clickTheGrid(a) {
    $(a).clone().appendTo(gridItemOpenInner), gridItemOpenInner.children(".event-grid-item").removeClass("event").addClass("in-focus").removeAttr("onclick");
    var b = gridItemOpenInner.find(".less-button");
    gridItemOpenInner.find(".more-button").toggleClass("open"), gridItemOpenHolder.toggleClass("opened"), 
    theBody.toggleClass("noscroll"), b.toggleClass("closed").click(function(a) {
        a.preventDefault(), gridItemOpenInner.empty(), gridItemOpenHolder.toggleClass("opened"), 
        theBody.toggleClass("noscroll");
    });
}

function startCaro() {
    var a = carouselItems.length;
    a > 0 && (window.innerWidth >= mediumScreen ? caroInverval ? (clearInterval(caroInverval), 
    caroInverval = setInterval(function() {
        caroMovement(a);
    }, 6e3)) : caroInverval = setInterval(function() {
        caroMovement(a);
    }, 6e3) : window.innerWidth < mediumScreen && caroInverval && (clearInterval(caroInverval), 
    $(carouselItems[0]).css("margin-left", 0)));
}

function caroMovement(a) {
    var b = (carouselItems[0].style.marginLeft, -1 * parseInt($(carouselItems[0]).css("margin-left"))), c = carouselItems[0].offsetWidth;
    c * (a - 1) > b ? $(carouselItems[0]).animate({
        marginLeft: "-=100%"
    }, 700) : b == c * (a - 1) && ($(carouselItems[0]).clone().appendTo(caroWrapper), 
    $(".carousel-item:last-child").css("margin-left", 0), $(carouselItems[0]).animate({
        marginLeft: "-=100%"
    }, 700, function() {
        $(carouselItems[0]).css("margin-left", 0), $(".carousel-item:last-child").remove();
    }));
}

function videoResize() {
    screenWidth = window.innerWidth;
    var a = largeVideoHolder.width(), b = Math.round(a / 16 * 9);
    largeIframe.height(b), a = smlVideoHolder.width(), b = Math.round(a / 16 * 9), smlVideoHolder.height(b), 
    startCaro();
}

var theBody = $("body"), theMenuButton = $("#menuButton"), mainNav = $("nav"), mainNavBtns = $(".top-level li a"), underNav = $(".undernav"), underNavItems = $(".undernav .undernav-item"), underNavClose = $("#underNavCloseBtn"), onTourBtn = $("#onTourBtn"), offTourBtn = $("#offTourBtn"), onTourList = $("#onTourList"), moreSubNav = $("#moreSubNav"), lessSubNav = $("#lessSubNav"), subNavExtras = $("#subNavExtras"), mediaHolder = $("#mediaHolder"), largeVideoHolder = $("#largeVideoHolder"), largeIframe = $("#largeVideoHolder iframe"), smlVideoHolder = $("#smallVideoHolder"), smallScreen = 480, mediumScreen = 790, largeScreen = 1024, xlargeScreen = 1440, screenWidth = window.innerWidth, sideScene, scrollController = new ScrollMagic.Controller(), dateSlider = $("#dateSlider"), whatsonSlider = $("#whatsonSlider"), changeSelections = $("#changeSelections"), woQuestions = $("#woQuestions"), woQClose = $("#woQCloseBtn"), woConfirmSelection = $("#woConfirmSelection"), questionHolder = $("#questionHolder"), whereBtns = $(".arrow-btn.where-btn"), whatBtns = $(".arrow-btn.what-btn"), whenBtns = $(".arrow-btn.when-btn"), optionItems = $(".option-item"), filterHolder = $(".allthefilters"), gridMoreBtns = $(".event-grid-item"), gridMoreActualBtns = $(".event-grid-item .more-button"), gridLessBtns = $(".event-grid-item .less-button"), gridItemOpenHolder = $("#gridItemOpenHolder"), gridItemOpenInner = $("#gridItemOpenInner"), caroInverval, carouselImages = $(".homepage-carousel .carousel-item .main-image"), carouselItems = $(".homepage-carousel .carousel-item"), caroWrapper = $(".homepage-carousel-wrapper");

$(function() {
    if (mainNavBtns.each(function(a) {
        $(this).click(function(b) {
            "What's On &amp; Book Tickets" == $(this).html() || "Shopping" == $(this).html() || "Rooftop Restaurant" == $(this).html() || (b.preventDefault(), 
            theBody.addClass("noscroll"), underNav.toggleClass("opened"), $(underNavItems[a]).toggleClass("vis"));
        });
    }), underNavClose.click(function(a) {
        a.preventDefault(), theBody.removeClass("noscroll"), underNav.toggleClass("opened"), 
        underNavItems.removeClass("vis");
    }), theMenuButton.click(function(a) {
        a.preventDefault(), mainNav.toggleClass("nav-active"), theBody.toggleClass("noscroll");
    }), onTourBtn.click(function(a) {
        a.preventDefault(), onTourList.toggleClass("on-tour-active"), onTourBtn.toggleClass("open"), 
        offTourBtn.toggleClass("closed"), subNavExtras.hasClass("sub-nav-active") && (subNavExtras.toggleClass("sub-nav-active"), 
        lessSubNav.toggleClass("closed"), moreSubNav.toggleClass("open"));
    }), offTourBtn.click(function(a) {
        a.preventDefault(), onTourList.toggleClass("on-tour-active"), onTourBtn.toggleClass("open"), 
        offTourBtn.toggleClass("closed");
    }), moreSubNav.click(function(a) {
        a.preventDefault(), subNavExtras.toggleClass("sub-nav-active"), lessSubNav.toggleClass("closed"), 
        moreSubNav.toggleClass("open"), onTourList.hasClass("on-tour-active") && (onTourList.toggleClass("on-tour-active"), 
        onTourBtn.toggleClass("open"), offTourBtn.toggleClass("closed"));
    }), lessSubNav.click(function(a) {
        a.preventDefault(), subNavExtras.toggleClass("sub-nav-active"), lessSubNav.toggleClass("closed"), 
        moreSubNav.toggleClass("open");
    }), carouselImages.each(function(a) {
        $(this).click(function(b) {
            if (b.preventDefault(), window.innerWidth < mediumScreen) {
                $(this).siblings(".side-info").toggleClass("active"), $(this).parents(".carousel-item").toggleClass("in-focus");
                var c = $(".homepage-carousel .carousel-item");
                c.splice(a, 1);
                for (var d = 0; d < c.length; d++) $(c[d]).hasClass("in-focus") && ($(c[d]).removeClass("in-focus"), 
                $(c[d]).children(".side-info").removeClass("active"));
                $("html, body").animate({
                    scrollTop: $(this).offset().top - 77
                }, 500);
            }
        });
    }), startCaro(), changeSelections.click(function(a) {
        a.preventDefault(), woQuestions.toggleClass("opened"), theBody.addClass("noscroll");
    }), woQClose.click(function(a) {
        a.preventDefault(), woQuestions.toggleClass("opened"), theBody.removeClass("noscroll");
    }), woConfirmSelection.click(function(a) {
        a.preventDefault(), woQuestions.toggleClass("opened"), theBody.removeClass("noscroll");
    }), whereBtns.each(function() {
        $(this).click(function(a) {
            questionHolder.removeClass("what-pos").removeClass("when-pos").addClass("where-pos");
        });
    }), whatBtns.each(function() {
        $(this).click(function(a) {
            questionHolder.removeClass("where-pos").removeClass("when-pos").addClass("what-pos");
        });
    }), whenBtns.each(function() {
        $(this).click(function(a) {
            questionHolder.removeClass("what-pos").removeClass("where-pos").addClass("when-pos");
        });
    }), optionItems.each(function() {
        $(this).click(function(a) {
            $(this).hasClass("show-all") ? $(this).hasClass("option-selected") ? ($(this).siblings(".option-item:not(.cinemas)").toggleClass("option-selected"), 
            $(this).toggleClass("option-selected")) : ($(this).siblings(".option-item:not(.cinemas):not(.option-selected)").toggleClass("option-selected"), 
            $(this).toggleClass("option-selected"), $(this).siblings(".cinemas").removeClass("option-selected")) : $(this).hasClass("cinemas") ? ($(this).siblings(".option-item").removeClass("option-selected"), 
            $(this).toggleClass("option-selected")) : ($(this).siblings(".show-all").hasClass("option-selected") && $(this).siblings(".show-all").removeClass("option-selected"), 
            $(this).toggleClass("option-selected"), $(this).siblings(".cinemas").removeClass("option-selected"));
            var b = [], c = $(".options .option-selected"), d = 0;
            $("#titleSearch").val("").change(), c.each(function() {
                var a = $(this).attr("id"), c = $("#result #" + a);
                c.val(1), c.change(), $(this).hasClass("location-item") && d++;
                var e = $(this).html();
                $(this).hasClass("show-all") || b.push("<div class='filter-option filter-button selected' data-name='" + a + "'>" + e + "</div>");
            }), $("#datefilteroption").length > 0 && b.push($("[data-name='daterange']")), filterHolder.html(b);
            var e = $(".option-item:not(.option-selected)");
            e.each(function() {
                var a = $(this).attr("id"), b = $("#result #" + a);
                b.val(0), b.change();
            }), 1 > d && ($("#result #stratford").val(1).change(), $("#result #ontour").val(1).change(), 
            $("#result #london").val(1).change());
        });
    }), gridMoreBtns.each(function(a) {
        $(this).click(function(a) {
            a.preventDefault(), clickTheGrid();
        });
    }), gridMoreActualBtns.each(function(a) {
        $(this).click(function(a) {
            a.preventDefault();
        });
    }), videoResize(), $("#form").length > 0) {
        $.getJSON("js/json/pref-data.json", function(a) {
            var b = [], c = [];
            $.each(a, function(d) {
                b.push(moment(a[d].datestart, "MM DD YYYY").format("X")), c.push(moment(a[d].dateend, "MM DD YYYY").format("X"));
            }), e = Math.min.apply(Math, b), f = Math.max.apply(Math, c);
        }), $("#form").my({
            data: {
                sort: "dateorder",
                highlight: 1,
                ourplays: 0,
                education: 1,
                visitingcompanies: 1,
                liversc: 0,
                talks: 0,
                exhibitions: 0,
                family: 0,
                freeactivities: 0,
                schoolholidays: 0,
                comedy: 0,
                stratford: 1,
                london: 1,
                ontour: 1,
                usa: 1,
                china: 1,
                rangeMin: e,
                rangeMax: f,
                range: [],
                result: [],
                titleSearch: ""
            },
            require: [ {
                "this.List": {
                    url: "js/json/pref-data.json",
                    dataType: "json"
                }
            } ],
            init: function(a, b) {
                var c, d = this, g = b.data;
                g.range, g.highlight;
                c = d.List.map(function(a) {
                    return a.index = [ a.event, a.subtitle, a.highlight, a.ourplays, a.education, a.visitingcompanies, a.image, a.introcopy, a.readmore, a.buytickets, a.locations.join(", "), a.performance, a.datestart, a.dateend, a.performance, a.order ].join(" "), 
                    a;
                }), a.html(d.HTML.join(" ")), g.range = d.Dates = [ c.min("order").order, c.max("order").order ], 
                g.rangeMin = d.Datestart = e, g.rangeMax = d.Dateend = f;
            },
            ui: {
                "#highlight": {
                    bind: "highlight"
                },
                "#ourplays": {
                    bind: "ourplays"
                },
                "#education": {
                    bind: "education"
                },
                "#visitingcompanies": {
                    bind: "visitingcompanies"
                },
                "#liversc": {
                    bind: "liversc"
                },
                "#talks": {
                    bind: "talks"
                },
                "#exhibitions": {
                    bind: "exhibitions"
                },
                "#family": {
                    bind: "family"
                },
                "#freeactivities": {
                    bind: "freeactivities"
                },
                "#schoolholidays": {
                    bind: "schoolholidays"
                },
                "#comedy": {
                    bind: "comedy"
                },
                "#stratford": {
                    bind: "stratford"
                },
                "#london": {
                    bind: "london"
                },
                "#ontour": {
                    bind: "ontour"
                },
                "#usa": {
                    bind: "usa"
                },
                "#china": {
                    bind: "china"
                },
                "#rangeMin": {
                    bind: "rangeMin"
                },
                "#rangeMax": {
                    bind: "rangeMax"
                },
                "#titleSearch": {
                    bind: "titleSearch"
                },
                "#result": {
                    watch: "#highlight,#ourplays,#education,#visitingcompanies,#stratford,#london,#ontour,#usa,#china,#rangeMin,#rangeMax,#titleSearch",
                    bind: function(a, b, c) {
                        var d, e = this, f = c.my("find", "#theBody"), g = (a.range, a.highlight), h = a.ourplays, i = a.education, j = a.stratford, k = a.london, l = a.ontour, m = a.rangeMax, n = a.rangeMin, o = a.titleSearch, p = a.visitingcompanies, q = this.List.filter(function(a) {
                            return a.datestart = moment(a.datestart, [ "MM-DD-YYYY", "ddd D MMM", "X" ]).format("X"), 
                            a.dateend = moment(a.dateend, [ "MM-DD-YYYY", "ddd D MMM", "X" ]).format("X"), "" == o ? (a.highlight == g || a.ourplays == h || a.education == i || a.visitingcompanies == p) && (a.datestart >= n && a.dateend <= m || a.datestart <= n && a.dateend <= m && a.dateend >= n || a.datestart >= n && a.datestart <= m && a.dateend >= m) && (a.stratford == j || a.london == k || a.ontour == l) : a.event == o;
                        });
                        f.html(q.sort(this.Sorters[a.sort]).reduce(function(a, b) {
                            return b.datestart = moment(b.datestart, [ "MM-DD-YYYY", "ddd D MMM", "X" ]).format("ddd D MMM"), 
                            b.dateend = moment(b.dateend, [ "MM-DD-YYYY", "ddd D MMM", "X" ]).format("ddd D MMM"), 
                            a + e.Row.assign(b);
                        }, "")), d = c.my("find", ".perf-list-holder ul"), d.each(function(a) {
                            thePerf = q[a].performance, $(this).html(thePerf.reduce(function(a, b) {
                                return b.date = moment(b.date, "DD-MM-YYYY").format("ddd D MMM"), a + e.Perf.assign(b);
                            }, ""));
                        }), q.length || f.html("<span class='no-results'>Unfortunately you found no results</span>");
                    },
                    init: function(a, b) {}
                }
            },
            Sorters: {
                dateorder: function(a, b) {
                    return a.order - b.order;
                }
            },
            HTML: [ '<div id="result"><input id="highlight" type="number" step="any" value="" name="highlight"></input><input id="ourplays" name="ourplays" type="number" step="any" value=""></input><input id="education" name="education" type="number" step="any" value=""></input><input id="visitingcompanies" name="visitingcompanies" type="number" step="any" value=""></input><input id="liversc" name="liversc" type="number" step="any" value=""></input><input id="talks" name="talks" type="number" step="any" value=""></input><input id="exhibitions" name="exhibitions" type="number" step="any" value=""></input><input id="family" name="family" type="number" step="any" value=""></input><input id="freeactivities" name="freeactivities" type="number" step="any" value=""></input><input id="schoolholidays" name="schoolholidays" type="number" step="any" value=""></input><input id="comedy" name="comedy" type="number" step="any" value=""></input><input id="stratford" name="stratford" type="number" step="any" value=""></input><input id="london" name="london" type="number" step="any" value=""></input><input id="ontour" name="ontour" type="number" step="any" value=""></input><input id="usa" name="usa" type="number" step="any" value=""></input><input id="china" name="china" type="number" step="any" value=""></input><input id="rangeMin" name="rangeMin" type="number" step="any" value=""></input><input id="rangeMax" name="rangeMax" type="number" step="any" value=""></input><input id="titleSearch" name="titleSearch" type="text" step="any" value=""></input></div>', '<div id="theBody">', "</div>" ],
            Row: '<div class="event event-grid-item" onclick="clickTheGrid(this)"><header class="event-header"><img src="img/{image}" alt=""><div class="top-info"><div class="dates-places"><div class="dates">{datestart} to {dateend}</div><div class="places">{locations}</div></div> <!-- dates places --><div class="more-holder"><div href="" id="moreWhatsonEvent" class="more-button">More</div><a href="" id="lessWhatsonEvent" class="less-button closed">Less</a></div> <!-- more holder --> </div> <!-- top info --><div class="title-copy"><div class="title-bkgrnd"><div class="bkgrnd-strip"></div><h4 class="title">{event}</h4><p class="subtitle">{subtitle}</p></div> <!-- title bkgrnd --></div> <!-- title --></header> <!-- event header --><div class="bottom-info"><section class="main-info"><p class="intro-text">{introcopy}</p> <!-- intro text --><div class="button-holder"><a href="{readmore}" class="read-more">READ MORE</a></div> <!-- button holder --></section> <!-- main info --><section class="event-locations"><h5>See it at these locations:</h5><div class="perf-list-holder"><ul></ul></div></section> <!-- locations --><section class="event-related"><div></div><div></div><div></div></section> <!-- related --></div> <!-- bottom info --></div> <!-- event, grid item, generic -->',
            Perf: '<li data-matinee={matinee}><div class="performance"><div class="perf-option date-time"><div class="date">{date}</div><div class="time">{time}</div></div> <!-- perf option --><div class="perf-option location"><div class="city">{place}</div></div> <!-- perf option --></div> <!-- performance --><div class="buy-button"><a href="{buytickets}">Buy Tickets</a></div></li> <!-- performance li -->'
        }), $.getJSON("js/json/pref-data.json", function(a) {
            var b = [], c = [];
            $.each(a, function(d) {
                b.push(moment(a[d].datestart, "MM DD YYYY").format("X")), c.push(moment(a[d].dateend, "MM DD YYYY").format("X"));
            }), e = Math.min.apply(Math, b), f = Math.max.apply(Math, c);
            var d = $("#whatsonSlider");
            d.ionRangeSlider({
                min: moment(e, "X").format("X"),
                max: moment(f, "X").format("X"),
                from: moment(e, "X"),
                prettify: function(a) {
                    return moment(a, "X").format("D MMM");
                },
                step: 9e5
            }), d.on("change", function() {
                var a = $(this), b = a.prop("value").split(";");
                if ($("#rangeMin").val(b[0]).change(), $("#rangeMax").val(b[1]).change(), $("#titleSearch").val("").change(), 
                $("#datefilteroption").length > 0) $("#datefilteroption").html(moment(b[0], "X").format("D MMM") + " to " + moment(b[1], "X").format("D MMM")); else {
                    var c = "<div class='filter-option filter-button selected' data-name='daterange' id='datefilteroption'>" + moment(b[0], "X").format("D MMM") + " to " + moment(b[1], "X").format("D MMM") + "</div>";
                    $(".allthefilters").append(c);
                }
            });
        });
        var a = $("#searchButton"), b = $(".searchBox"), c = $(".searchBox button");
        a.click(function() {
            b.toggleClass("hidden");
        }), c.click(function() {
            $("#titleSearch").val("The Jew of Malta").change();
        });
    }
    if ($("#buy-tickets-form").length > 0) {
        var d, e, f, g = $("#buy-tickets-form").attr("class");
        "Loves" == g ? d = "js/json/loves_perfs.json" : "Merchant" == g ? d = "js/json/merchant_perfs.json" : "Othello" == g ? d = "js/json/othello_perfs.json" : "Henry" == g ? d = "js/json/henry_perfs.json" : "Wendy" == g ? d = "js/json/wendy_perfs.json" : "Malta" == g ? d = "js/json/malta_perfs.json" : "Famous" == g ? d = "js/json/famous_perfs.json" : "Volpone" == g ? d = "js/json/volpone_perfs.json" : "Hecuba" == g ? d = "js/json/hecuba_perfs.json" : "LoveFor" == g ? d = "js/json/lovefor_perfs.json" : "Queen" == g ? d = "js/json/queen_perfs.json" : "Death" == g && (d = "js/json/death_perfs.json"), 
        $.getJSON(d, function(a) {
            e = moment(a[0].date, "DD-MM-YYYY").format("X"), f = moment(a[a.length - 1].date, "DD-MM-YYYY").format("X");
        }), $("#buy-tickets-form").my({
            data: {
                showname: g,
                stratford: 1,
                london: 1,
                ontour: 1,
                incinemas: 0,
                evening: 1,
                matinee: 1,
                rangeMin: e,
                rangeMax: f,
                result: []
            },
            require: [ {
                "this.List": {
                    url: d,
                    dataType: "json"
                }
            } ],
            init: function(a, b) {
                var c, d = this, g = b.data;
                g.range, g.highlight;
                c = d.List.map(function(a) {
                    return a.index = [ a.theatre, a.place, a.date, a.matinee, a.evening, a.time, a.buytickets ].join(" "), 
                    a;
                }), a.html(d.HTML.join(" ")), g.rangeMin = d.Datestart = e, g.rangeMax = d.Dateend = f;
            },
            ui: {
                "#showname": {
                    bind: "showname"
                },
                "#stratford": {
                    bind: "stratford"
                },
                "#london": {
                    bind: "london"
                },
                "#ontour": {
                    bind: "ontour"
                },
                "#incinemas": {
                    bind: "incinemas"
                },
                "#evening": {
                    bind: "evening"
                },
                "#matinee": {
                    bind: "matinee"
                },
                "#rangeMin": {
                    bind: "rangeMin"
                },
                "#rangeMax": {
                    bind: "rangeMax"
                },
                "#result": {
                    watch: "#stratford,#london,#ontour,#incinemas,#evening,#matinee,#rangeMax,#rangeMin",
                    bind: function(a, b, c) {
                        var d = this, e = c.my("find", "ul#thePerfs"), f = (a.range, a.showname, a.stratford), g = a.london, h = a.ontour, i = a.evening, j = a.matinee, k = this.List.filter(function(a) {
                            return a.date = moment(a.date, [ "DD-MM-YYYY", "ddd D MMM", "X" ]).format("X"), 
                            a.date <= $(rangeMax).val() && a.date >= $(rangeMin).val() && (a.stratford == f || a.london == g || a.ontour == h) && (a.evening == i || a.matinee == j);
                        });
                        e.html(k.reduce(function(a, b) {
                            return b.date = moment(b.date, [ "DD-MM-YYYY", "ddd D MMM", "X" ]).format("ddd D MMM"), 
                            a + d.Row.assign(b);
                        }, "")), k.length || e.html("<span class='no-results'>Unfortunately you found no results</span>");
                    }
                }
            },
            HTML: [ '<div id="result"><input id="showname" type="text" step="any" value="" name="showname"></input><input id="incinemas" name="incinemas" type="number" step="any" value=""></input><input id="evening" name="evening" type="number" step="any" value=""></input><input id="matinee" name="matinee" type="number" step="any" value=""></input><input id="stratford" name="stratford" type="number" step="any" value=""></input><input id="london" name="london" type="number" step="any" value=""></input><input id="ontour" name="ontour" type="number" step="any" value=""></input><input id="rangeMin" name="rangeMin" type="number" step="any" value=""></input><input id="rangeMax" name="rangeMax" type="number" step="any" value=""></input></div>', '<ul id="thePerfs">', "</ul>" ],
            Row: '<li data-matinee={matinee}><div class="performance"><div class="perf-option date-time"><div class="date">{date}</div><div class="time">{time}</div></div> <!-- perf option --><div class="perf-option location"><div class="city">{place}</div><div class="city">, {theatre}</div></div> <!-- perf option --><div class="perf-option extra-info">{extrainfo}</div></div> <!-- performance --><div class="buy-button"><a href="finish.html">Buy Tickets</a></div></li> <!-- performance li -->'
        }), $.getJSON(d, function(a) {
            e = a[0].date, f = a[a.length - 1].date;
            var b = $("#dateSlider");
            b.ionRangeSlider({
                min: moment(e, "DD-MM-YYYY").format("X"),
                max: moment(f, "DD-MM-YYYY").format("X"),
                from: moment(e, "DD-MM-YYYY").format("X"),
                prettify: function(a) {
                    return moment(a, "X").format("D MMM");
                }
            }), b.on("change", function() {
                var a = $(this), b = a.prop("value").split(";");
                $("#rangeMin").val(b[0]).change(), $("#rangeMax").val(b[1]).change();
            });
            var c = 0, d = 0, g = 0;
            for (var h in a) a[h].stratford && c++, a[h].london && d++, a[h].ontour && g++;
            1 > c && $("#suaCheck").attr("disabled", !0).removeAttr("checked").parent(".filter-option").addClass("disabled"), 
            1 > d && $("#londonCheck").attr("disabled", !0).removeAttr("checked").parent(".filter-option").addClass("disabled"), 
            1 > g && $("#onTourCheck").attr("disabled", !0).removeAttr("checked").parent(".filter-option").addClass("disabled");
        });
        var h = $("#suaCheck");
        h.click(function() {
            $(this).is(":checked") ? $("#stratford").val(1).change() : $("#stratford").val(0).change();
        });
        var i = $("#londonCheck");
        i.click(function() {
            $(this).is(":checked") ? $("#london").val(1).change() : $("#london").val(0).change();
        });
        var j = $("#onTourCheck");
        j.click(function() {
            $(this).is(":checked") ? $("#ontour").val(1).change() : $("#ontour").val(0).change();
        });
        var k = $("#eveningCheck");
        k.click(function() {
            $(this).is(":checked") ? $("#evening").val(1).change() : $("#evening").val(0).change();
        });
        var l = $("#matineeCheck");
        l.click(function() {
            $(this).is(":checked") ? $("#matinee").val(1).change() : $("#matinee").val(0).change();
        });
    }
}), window.onresize = videoResize;