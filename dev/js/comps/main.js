var theBody = $("body"),
    theMenuButton = $("#menuButton"),
    mainNav = $("nav"), mainNavBtns = $(".top-level li a"), underNav = $(".undernav"), underNavItems = $(".undernav .undernav-item"), underNavClose = $("#underNavCloseBtn"),
    onTourBtn = $("#onTourBtn"), offTourBtn = $("#offTourBtn"),
    onTourList = $("#onTourList"),
    moreSubNav = $("#moreSubNav"), lessSubNav = $("#lessSubNav"),
    subNavExtras = $("#subNavExtras"),
    mediaHolder = $("#mediaHolder"), largeVideoHolder = $("#largeVideoHolder"), largeIframe = $("#largeVideoHolder iframe"), smlVideoHolder = $("#smallVideoHolder"),
    smallScreen = 480, mediumScreen = 790, largeScreen = 1024, xlargeScreen = 1440,
    screenWidth = window.innerWidth,
    sideScene, scrollController = new ScrollMagic.Controller(),
    dateSlider = $("#dateSlider"), whatsonSlider = $("#whatsonSlider"),
    changeSelections = $("#changeSelections"), woQuestions = $("#woQuestions"), woQClose = $("#woQCloseBtn"), woConfirmSelection = $("#woConfirmSelection"), questionHolder = $("#questionHolder"),
    whereBtns = $(".arrow-btn.where-btn"), whatBtns = $(".arrow-btn.what-btn"), whenBtns = $(".arrow-btn.when-btn"),
    optionItems = $(".option-item"), filterHolder = $(".allthefilters"),
    gridMoreBtns = $(".event-grid-item"), gridMoreActualBtns = $(".event-grid-item .more-button"), gridLessBtns = $(".event-grid-item .less-button"), gridItemOpenHolder = $("#gridItemOpenHolder"), gridItemOpenInner = $("#gridItemOpenInner"),
    caroInverval, carouselImages = $(".homepage-carousel .carousel-item .main-image"), carouselItems = $(".homepage-carousel .carousel-item"), caroWrapper = $(".homepage-carousel-wrapper");

$(function() {
    
    /*---------- NAV CLICK FUNCTIONS ----------*/
    
    // SUB NAV SHOW
    mainNavBtns.each(function(i){
        $(this).click(function(e){
            if($(this).html() == "What's On &amp; Book Tickets" || $(this).html() == "Shopping" || $(this).html() == "Rooftop Restaurant") {
                // do nothing
            } else {
                e.preventDefault();
                theBody.addClass("noscroll");
                underNav.toggleClass("opened");
                $(underNavItems[i]).toggleClass("vis");
            }
        });
    });
    underNavClose.click(function(e){
        e.preventDefault();
        theBody.removeClass("noscroll");
        underNav.toggleClass("opened");
        underNavItems.removeClass("vis");
    });
    // MENU BUTTON FUNCTION
    theMenuButton.click(function(e){
        e.preventDefault();
        mainNav.toggleClass("nav-active");
        theBody.toggleClass("noscroll");
    });
    
    // ON TOUR ON CLICK
    onTourBtn.click(function(e){
        e.preventDefault();
        onTourList.toggleClass("on-tour-active");
        onTourBtn.toggleClass("open");
        offTourBtn.toggleClass("closed");
        if (subNavExtras.hasClass("sub-nav-active")){
            subNavExtras.toggleClass("sub-nav-active");
            lessSubNav.toggleClass("closed");
            moreSubNav.toggleClass("open");
        }
    });
    
    offTourBtn.click(function(e){
        e.preventDefault();
        onTourList.toggleClass("on-tour-active");
        onTourBtn.toggleClass("open");
        offTourBtn.toggleClass("closed");
    });
    
    // + MORE ON CLICK
    moreSubNav.click(function(e){
        e.preventDefault();
        subNavExtras.toggleClass("sub-nav-active");
        lessSubNav.toggleClass("closed");
        moreSubNav.toggleClass("open");
        if (onTourList.hasClass("on-tour-active")){
            onTourList.toggleClass("on-tour-active");
            onTourBtn.toggleClass("open");
            offTourBtn.toggleClass("closed");
        }
    });
    
    lessSubNav.click(function(e){
        e.preventDefault();
        subNavExtras.toggleClass("sub-nav-active");
        lessSubNav.toggleClass("closed");
        moreSubNav.toggleClass("open");
    });
    
    
    
    /*---------- HOMEPAGE CAROUSEL ----------*/
    
    // small screen click function
    
    carouselImages.each(function(i){
        $(this).click(function(e){
            e.preventDefault();
            if (window.innerWidth < mediumScreen){ // small screens, no carousel
                $(this).siblings(".side-info").toggleClass("active");
                $(this).parents(".carousel-item").toggleClass("in-focus");
                
                var otherItems = $(".homepage-carousel .carousel-item");
                otherItems.splice(i,1);
                
                for (var j = 0; j < otherItems.length; j++){
                    if ($(otherItems[j]).hasClass("in-focus")){
                        $(otherItems[j]).removeClass("in-focus");
                        $(otherItems[j]).children(".side-info").removeClass("active");
                    }
                }
                
                $('html, body').animate({
                    scrollTop: ($(this).offset().top - 77)
                }, 500);
            }
        });
    });
    
    startCaro();
    
    
    
    /*---------- WHAT'S ON QUESTIONS ----------*/
    
    changeSelections.click(function(e) {
        e.preventDefault();
        woQuestions.toggleClass("opened");
        theBody.addClass("noscroll");
    });
    
    woQClose.click(function(e) {
        e.preventDefault();
        woQuestions.toggleClass("opened");
        theBody.removeClass("noscroll");
    });
    
    woConfirmSelection.click(function(e) {
        e.preventDefault();
        woQuestions.toggleClass("opened");
        theBody.removeClass("noscroll");
    });
    
    
    //  QUESTION BUTTONS
    
     whereBtns.each(function() {
         $(this).click(function(e) {
             questionHolder.removeClass("what-pos").removeClass("when-pos").addClass("where-pos");
         });
     });
     
     whatBtns.each(function() {
         $(this).click(function(e) {
             questionHolder.removeClass("where-pos").removeClass("when-pos").addClass("what-pos");
         });
     });
     
     whenBtns.each(function() {
         $(this).click(function(e) {
             questionHolder.removeClass("what-pos").removeClass("where-pos").addClass("when-pos");
         });
     });
    
    
    // OPTION ITEMS
    
    optionItems.each(function() {
        $(this).click(function(e) {
            if ($(this).hasClass("show-all")){
                if ($(this).hasClass("option-selected")) {
                    $(this).siblings(".option-item:not(.cinemas)").toggleClass("option-selected");
                    $(this).toggleClass("option-selected");
                } else {
                    $(this).siblings(".option-item:not(.cinemas):not(.option-selected)").toggleClass("option-selected");
                    $(this).toggleClass("option-selected");
                    $(this).siblings(".cinemas").removeClass("option-selected");
                }
            } else if ($(this).hasClass("cinemas")) {
                $(this).siblings(".option-item").removeClass("option-selected");
                $(this).toggleClass("option-selected");
            } else {
                if ($(this).siblings(".show-all").hasClass("option-selected")) {
                    $(this).siblings(".show-all").removeClass("option-selected");
                }
                $(this).toggleClass("option-selected");
                $(this).siblings(".cinemas").removeClass("option-selected");
            }
            
            var newFilter = [], selectedOnes = $(".options .option-selected"), locationSelected = 0;
            $("#titleSearch").val("").change();
            
            selectedOnes.each(function(){
                var theId = $(this).attr("id");
                var theBox = $("#result #" + theId);
                theBox.val(1);
                theBox.change();
                
                if ($(this).hasClass("location-item")) {
                    locationSelected++;
                }
                
                var theCopy = $(this).html();
                if (!$(this).hasClass("show-all")){
                    newFilter.push("<div class='filter-option filter-button selected' data-name='" + theId + "'>" + theCopy + "</div>");
                }
                
            });
            
            if ($("#datefilteroption").length>0){
                newFilter.push($("[data-name='daterange']"));
            }
            filterHolder.html(newFilter);
            
            var notSelectedOnes = $(".option-item:not(.option-selected)");
            notSelectedOnes.each(function(){
                var theId = $(this).attr("id");
                var theBox = $("#result #" + theId);
                theBox.val(0);
                theBox.change();
            });
            
            if (locationSelected < 1) {
                $("#result #stratford").val(1).change();
                $("#result #ontour").val(1).change();
                $("#result #london").val(1).change();
            }
            
        });
    });
    
    
    // READ MORE
    
    gridMoreBtns.each(function(i){
        $(this).click(function(e) {
            e.preventDefault();
            clickTheGrid()
        });
            /* 
            $(this).clone().appendTo(gridItemOpenInner);
            gridItemOpenInner.children(".event-grid-item").removeClass("event").addClass("in-focus");
            var lessButton = gridItemOpenInner.find(".less-button");
            gridItemOpenInner.find(".more-button").toggleClass("open");
            gridItemOpenHolder.toggleClass("opened");
            theBody.toggleClass("noscroll");
            
            lessButton.toggleClass("closed").click(function(e){
                e.preventDefault();
                gridItemOpenInner.empty();
                gridItemOpenHolder.toggleClass("opened");
                theBody.toggleClass("noscroll");
            });
        }); */
    });
    gridMoreActualBtns.each(function(i){
        $(this).click(function(e){
            e.preventDefault();
        });
    });
    
    /*---------- RESIZING FUNCTIONS ----------*/
    
    videoResize();
    
    
    /*---------- WHATSON FORM SETTER ----------*/
    
    if ($("#form").length>0){
        $.getJSON( "js/json/pref-data.json", function( data ) {
            var dateStartArray = [];
            var dateEndArray = [];
            
            $.each(data, function(i){
                dateStartArray.push(moment(data[i].datestart, "MM DD YYYY").format("X"));
                dateEndArray.push(moment(data[i].dateend, "MM DD YYYY").format("X"));
            });
            
            dateStart = Math.min.apply(Math,dateStartArray);
            dateEnd = Math.max.apply(Math,dateEndArray);
        });
        
        $("#form").my({
            data:{
                sort: "dateorder",
                highlight: 1,
                ourplays: 0,
                education: 1,
                visitingcompanies: 1,
                liversc: 0,
                talks: 0,
                exhibitions: 0,
                family: 0,
                freeactivities: 0,
                schoolholidays: 0,
                comedy: 0,
                stratford: 1,
                london: 1,
                ontour: 1,
                usa: 1,
                china: 1,
                rangeMin: dateStart,
                rangeMax: dateEnd,
                range: [],
                result: [],
                titleSearch: "",
            },
            require: [
                {
                    "this.List":{
                        url: "js/json/pref-data.json",
                        dataType: "json"
                    }
                }
            ],
            init: function($form, form){
                var that = this,
                    d = form.data,
                    list, $theBody, $perfs,
                    range = d.range,
                    highlight = d.highlight;

                list = that.List.map(function(e){
                    e.index=[e.event, e.subtitle, e.highlight, e.ourplays, e.education, e.visitingcompanies, e.image, e.introcopy, e.readmore, e.buytickets, e.locations.join(", "), e.performance, e.datestart, e.dateend, e.performance, e.order].join(" ");
                    return e;
                });

                $form.html(that.HTML.join(" "));
                
                d.range = that.Dates = [list.min("order").order, list.max("order").order];
                
                d.rangeMin = that.Datestart = dateStart
                d.rangeMax = that.Dateend = dateEnd;
                
            },
            ui : {
                "#highlight":{
                    bind: "highlight"
                },
                "#ourplays":{
                    bind: "ourplays"
                },
                "#education":{
                    bind: "education"
                },
                "#visitingcompanies":{
                    bind: "visitingcompanies"
                },
                "#liversc":{
                    bind: "liversc"
                },
                "#talks":{
                    bind: "talks"
                },
                "#exhibitions":{
                    bind: "exhibitions"
                },
                "#family":{
                    bind: "family"
                },
                "#freeactivities":{
                    bind: "freeactivities"
                },
                "#schoolholidays":{
                    bind: "schoolholidays"
                },
                "#comedy":{
                    bind: "comedy"
                },
                "#stratford":{
                    bind: "stratford"
                },
                "#london":{
                    bind: "london"
                },
                "#ontour":{
                    bind: "ontour"
                },
                "#usa":{
                    bind: "usa"
                },
                "#china":{
                    bind: "china"
                },
                "#rangeMin":{
                    bind: "rangeMin"
                },
                "#rangeMax":{
                    bind: "rangeMax"
                },
                "#titleSearch":{
                    bind: "titleSearch"
                },
                "#result":{
                    watch: "#highlight,#ourplays,#education,#visitingcompanies,#stratford,#london,#ontour,#usa,#china,#rangeMin,#rangeMax,#titleSearch",
                    bind: function(data, value, $node){
                        var that = this,
                            $theBody = $node.my("find","#theBody"),
                            range = data.range,
                            highlight = data.highlight,
                            ourplays = data.ourplays,
                            education = data.education,
                            stratford = data.stratford,
                            london = data.london,
                            ontour = data.ontour,
                            rangeMax = data.rangeMax,
                            rangeMin = data.rangeMin,
                            title = data.titleSearch,
                            visitingcompanies = data.visitingcompanies,
                            $perfs;
                        
                        var a = this.List.filter(function(e){
                            e.datestart = moment(e.datestart, ["MM-DD-YYYY", "ddd D MMM", "X"]).format("X");
                            e.dateend = moment(e.dateend, ["MM-DD-YYYY", "ddd D MMM", "X"]).format("X");
                            if (title == ""){
                                return (e.highlight == highlight || e.ourplays == ourplays || e.education == education || e.visitingcompanies == visitingcompanies) && ((e.datestart >= rangeMin && e.dateend <= rangeMax) || (e.datestart <= rangeMin && e.dateend <= rangeMax && e.dateend >= rangeMin) || (e.datestart >= rangeMin && e.datestart <= rangeMax && e.dateend >= rangeMax)) && (e.stratford == stratford || e.london == london || e.ontour == ontour);
                            } else {
                                return e.event == title;
                            }
                        });
                        
                        $theBody.html(
                            // 
                            a.sort(this.Sorters[data.sort]).reduce(
                                function(trail,e){
                                    e.datestart = moment(e.datestart, ["MM-DD-YYYY", "ddd D MMM", "X"]).format("ddd D MMM");
                                    e.dateend = moment(e.dateend, ["MM-DD-YYYY", "ddd D MMM", "X"]).format("ddd D MMM");
                                    return trail+that.Row.assign(e)
                                },
                                ""
                            )
                        );
                        
                        $perfs = $node.my("find",".perf-list-holder ul");
                        $perfs.each(function(i){
                            thePerf = a[i].performance;
                            $(this).html(
                                thePerf.reduce(
                                    function(trail,e){
                                        e.date = moment(e.date, "DD-MM-YYYY").format("ddd D MMM");
                                        return trail+that.Perf.assign(e)
                                    },
                                    ""
                                )
                            );
                        });
                        
                        if (!a.length){
                            
                            $theBody.html("<span class='no-results'>Unfortunately you found no results</span>");
                            
                        }
                        
                    },
                    init: function($form, form){
                        
                    }   
                }
            },
            // Sorter fuctions
            Sorters:{
                "dateorder": function(x,y){return x.order-y.order}
            },
            HTML: ['<div id="result"><input id="highlight" type="number" step="any" value="" name="highlight"></input><input id="ourplays" name="ourplays" type="number" step="any" value=""></input><input id="education" name="education" type="number" step="any" value=""></input><input id="visitingcompanies" name="visitingcompanies" type="number" step="any" value=""></input><input id="liversc" name="liversc" type="number" step="any" value=""></input><input id="talks" name="talks" type="number" step="any" value=""></input><input id="exhibitions" name="exhibitions" type="number" step="any" value=""></input><input id="family" name="family" type="number" step="any" value=""></input><input id="freeactivities" name="freeactivities" type="number" step="any" value=""></input><input id="schoolholidays" name="schoolholidays" type="number" step="any" value=""></input><input id="comedy" name="comedy" type="number" step="any" value=""></input><input id="stratford" name="stratford" type="number" step="any" value=""></input><input id="london" name="london" type="number" step="any" value=""></input><input id="ontour" name="ontour" type="number" step="any" value=""></input><input id="usa" name="usa" type="number" step="any" value=""></input><input id="china" name="china" type="number" step="any" value=""></input><input id="rangeMin" name="rangeMin" type="number" step="any" value=""></input><input id="rangeMax" name="rangeMax" type="number" step="any" value=""></input><input id="titleSearch" name="titleSearch" type="text" step="any" value=""></input></div>',
                   '<div id="theBody">',
                   '</div>'
                  ],
            Row: '<div class="event event-grid-item" onclick="clickTheGrid(this)">'
                +'<header class="event-header">'
                +'<img src="img/{image}" alt="">'
                +'<div class="top-info">'
                +'<div class="dates-places">'
                +'<div class="dates">{datestart} to {dateend}</div>'
                +'<div class="places">{locations}</div>'
                +'</div> <!-- dates places -->'
                +'<div class="more-holder">'
                +'<div href="" id="moreWhatsonEvent" class="more-button">More</div>'
                +'<a href="" id="lessWhatsonEvent" class="less-button closed">Less</a>'
                +'</div> <!-- more holder --> </div> <!-- top info -->'
                +'<div class="title-copy">'
                +'<div class="title-bkgrnd">'
                +'<div class="bkgrnd-strip"></div>'
                +'<h4 class="title">{event}</h4>'
                +'<p class="subtitle">{subtitle}</p>'
                +'</div> <!-- title bkgrnd -->'
                +'</div> <!-- title -->'
                +'</header> <!-- event header -->'
                +'<div class="bottom-info">'
                +'<section class="main-info">'
                +'<p class="intro-text">{introcopy}</p> <!-- intro text -->'
                +'<div class="button-holder">'
                +'<a href="{readmore}" class="read-more">READ MORE</a>'
                +'</div> <!-- button holder -->'
                +'</section> <!-- main info -->'
                +'<section class="event-locations">'
                +'<h5>See it at these locations:</h5>'
                +'<div class="perf-list-holder">'
                +'<ul>'
                +'</ul>'
                +'</div>'
                +'</section> <!-- locations -->'
                +'<section class="event-related">'
                +'<div></div>'
                +'<div></div>'
                +'<div></div>'
                +'</section> <!-- related -->'
                +'</div> <!-- bottom info -->'
                +'</div> <!-- event, grid item, generic -->',
            Perf: 
                 '<li data-matinee={matinee}>'
                +'<div class="performance">'
                +'<div class="perf-option date-time">'
                +'<div class="date">{date}</div>'
                +'<div class="time">{time}</div>'
                +'</div> <!-- perf option -->'
                +'<div class="perf-option location">'
                +'<div class="city">{place}</div>'
                +'</div> <!-- perf option -->'
                +'</div> <!-- performance -->'
                +'<div class="buy-button"><a href="{buytickets}">Buy Tickets</a></div>'
                +'</li> <!-- performance li -->'
        });
    
    
        /*---------- WHATSON SLIDER ----------*/
        $.getJSON( "js/json/pref-data.json", function( data ) {
            var dateStartArray = [];
            var dateEndArray = [];
            
            $.each(data, function(i){
                dateStartArray.push(moment(data[i].datestart, "MM DD YYYY").format("X"));
                dateEndArray.push(moment(data[i].dateend, "MM DD YYYY").format("X"));
            });
            
            dateStart = Math.min.apply(Math,dateStartArray);
            dateEnd = Math.max.apply(Math,dateEndArray);
            
            var whatsonSlider = $("#whatsonSlider");
            whatsonSlider.ionRangeSlider({
                min: moment(dateStart, "X").format("X"),
                max: moment(dateEnd, "X").format("X"),
                from: moment(dateStart, "X"),
                prettify: function (num) {
                    return moment(num, "X").format("D MMM");
                },
                step: 900000
            });
            
            whatsonSlider.on("change", function () {
                var $this = $(this),
                    value = $this.prop("value").split(";");

                $("#rangeMin").val(value[0]).change();
                $("#rangeMax").val(value[1]).change();
                $("#titleSearch").val("").change();
                
                if ($("#datefilteroption").length>0){
                    $("#datefilteroption").html(moment(value[0], "X").format("D MMM") + " to " + moment(value[1], "X").format("D MMM"));
                    
                } else {
                    var theOption = "<div class='filter-option filter-button selected' data-name='daterange' id='datefilteroption'>"
                                    + moment(value[0], "X").format("D MMM")
                                    + " to "
                                    + moment(value[1], "X").format("D MMM")
                                    + "</div>";
                    $(".allthefilters").append(theOption);
                }
            });
        });
    
    
        /*---------- WHATSON SEARCH ----------*/
        var searchButton = $("#searchButton"), searchBox = $(".searchBox"), searchGo = $(".searchBox button");
        searchButton.click(function(){
            searchBox.toggleClass("hidden");
        });
        searchGo.click(function(){
            $("#titleSearch").val("The Jew of Malta").change();
        });
    }
    
    
    
    /*---------- BUY TICKETS DATES SETUP ----------*/
    
    if ($("#buy-tickets-form").length>0){
        var theUrl, dateStart, dateEnd, theShow = $("#buy-tickets-form").attr('class');
        
        if (theShow == "Loves") {
            theUrl = "js/json/loves_perfs.json";
        } else if (theShow == "Merchant") {
            theUrl = "js/json/merchant_perfs.json";
        } else if (theShow == "Othello") {
            theUrl = "js/json/othello_perfs.json";
        } else if (theShow == "Henry") {
            theUrl = "js/json/henry_perfs.json";
        } else if (theShow == "Wendy") {
            theUrl = "js/json/wendy_perfs.json";
        } else if (theShow == "Malta") {
            theUrl = "js/json/malta_perfs.json";
        } else if (theShow == "Famous") {
            theUrl = "js/json/famous_perfs.json";
        } else if (theShow == "Volpone") {
            theUrl = "js/json/volpone_perfs.json";
        } else if (theShow == "Hecuba") {
            theUrl = "js/json/hecuba_perfs.json";
        } else if (theShow == "LoveFor") {
            theUrl = "js/json/lovefor_perfs.json";
        } else if (theShow == "Queen") {
            theUrl = "js/json/queen_perfs.json";
        } else if (theShow == "Death") {
            theUrl = "js/json/death_perfs.json";
        }
        
        $.getJSON( theUrl, function( data ) {
            dateStart = moment(data[0].date, "DD-MM-YYYY").format("X");
            dateEnd = moment(data[data.length-1].date, "DD-MM-YYYY").format("X");
        });
        
        $("#buy-tickets-form").my({
            data:{
                showname: theShow,
                stratford: 1,
                london: 1,
                ontour: 1,
                incinemas: 0,
                evening: 1,
                matinee: 1,
                rangeMin: dateStart,
                rangeMax: dateEnd,
                result: []
            },
            require: [
                {
                    "this.List":{
                        url: theUrl,
                        dataType: "json"
                    }
                }
            ],
            init: function($form, form){
                var that = this,
                    d = form.data,
                    list, $theBody, $perfs,
                    range = d.range,
                    highlight = d.highlight;

                list = that.List.map(function(e){
                    e.index=[e.theatre, e.place, e.date, e.matinee, e.evening, e.time, e.buytickets].join(" ");
                    return e;
                });
                
                //console.log("d= " + d);
                
                $form.html(that.HTML.join(" "));
                
                d.rangeMin = that.Datestart = dateStart
                d.rangeMax = that.Dateend = dateEnd;
                
            },
            ui : {
                "#showname": {
                    bind: "showname"
                },
                "#stratford":{
                    bind: "stratford"
                },
                "#london":{
                    bind: "london"
                },
                "#ontour":{
                    bind: "ontour"
                },
                "#incinemas":{
                    bind: "incinemas"
                },
                "#evening":{
                    bind: "evening"
                },
                "#matinee":{
                    bind: "matinee"
                },
                "#rangeMin":{
                    bind: "rangeMin"
                },
                "#rangeMax":{
                    bind: "rangeMax"
                },
                "#result":{
                    watch: "#stratford,#london,#ontour,#incinemas,#evening,#matinee,#rangeMax,#rangeMin",
                    bind: function(data, value, $node){
                        var that = this,
                            $theBody = $node.my("find","ul#thePerfs"),
                            range = data.range,
                            showname = data.showname,
                            stratford = data.stratford, london = data.london, ontour = data.ontour, evening = data.evening, matinee = data.matinee,
                            $perfs;
                        
                        var a = this.List.filter(function(e){
                            e.date = moment(e.date, ["DD-MM-YYYY", "ddd D MMM", "X"]).format("X");
                            return (e.date<=$(rangeMax).val() && e.date>=$(rangeMin).val()) && (e.stratford == stratford || e.london == london || e.ontour == ontour) && (e.evening == evening || e.matinee == matinee);
                        });
                        
                        $theBody.html(
                            a.reduce(
                                function(trail,e){
                                    e.date = moment(e.date, ["DD-MM-YYYY", "ddd D MMM", "X"]).format("ddd D MMM");
                                    return trail+that.Row.assign(e)
                                },
                                ""
                            )
                        );
                        
                        if (!a.length){
                            
                            $theBody.html("<span class='no-results'>Unfortunately you found no results</span>");
                            
                        }
                    }
                }
            },
            HTML: ['<div id="result"><input id="showname" type="text" step="any" value="" name="showname"></input><input id="incinemas" name="incinemas" type="number" step="any" value=""></input><input id="evening" name="evening" type="number" step="any" value=""></input><input id="matinee" name="matinee" type="number" step="any" value=""></input><input id="stratford" name="stratford" type="number" step="any" value=""></input><input id="london" name="london" type="number" step="any" value=""></input><input id="ontour" name="ontour" type="number" step="any" value=""></input><input id="rangeMin" name="rangeMin" type="number" step="any" value=""></input><input id="rangeMax" name="rangeMax" type="number" step="any" value=""></input></div>',
                   '<ul id="thePerfs">',
                   '</ul>'
                  ],
            Row: 
                 '<li data-matinee={matinee}>'
                +'<div class="performance">'
                +'<div class="perf-option date-time">'
                +'<div class="date">{date}</div>'
                +'<div class="time">{time}</div>'
                +'</div> <!-- perf option -->'
                +'<div class="perf-option location">'
                +'<div class="city">{place}</div>'
                +'<div class="city">, {theatre}</div>'
                +'</div> <!-- perf option -->'
                +'<div class="perf-option extra-info">{extrainfo}</div>'
                +'</div> <!-- performance -->'
                +'<div class="buy-button"><a href="finish.html">Buy Tickets</a></div>'
                +'</li> <!-- performance li -->'
        });
        
        /*---------- DATE SLIDER ----------*/
        
        $.getJSON( theUrl, function( data ) {
            dateStart = data[0].date;
            dateEnd = data[data.length-1].date; //+moment(data[data.length-1].date, "DD-MM-YYYY").format("X");
            
            var dateSlider = $("#dateSlider")
            dateSlider.ionRangeSlider({
                min: moment(dateStart, "DD-MM-YYYY").format("X"),
                max: moment(dateEnd, "DD-MM-YYYY").format("X"),
                from: moment(dateStart, "DD-MM-YYYY").format("X"),
                prettify: function (num) {
                    return moment(num, "X").format("D MMM");
                }
            });
            dateSlider.on("change", function () {
                var $this = $(this),
                    value = $this.prop("value").split(";");

                $("#rangeMin").val(value[0]).change();
                $("#rangeMax").val(value[1]).change();
            });
            
            // disable filters
            
            var stratVar = 0, londVar = 0, ontourVar = 0;
            
            for (var i in data){
                if (data[i].stratford){
                    stratVar++;
                }
                if (data[i].london) {
                    londVar++;
                } 
                if (data[i].ontour) {
                    ontourVar++;
                }
            }
                        
            if (stratVar < 1) {
                $("#suaCheck").attr("disabled", true).removeAttr("checked").parent(".filter-option").addClass("disabled");
            }

            if (londVar < 1) {
                $("#londonCheck").attr("disabled", true).removeAttr("checked").parent(".filter-option").addClass("disabled");
            }

            if (ontourVar < 1) {
                $("#onTourCheck").attr("disabled", true).removeAttr("checked").parent(".filter-option").addClass("disabled");
            }
            
        });
        
        var stratfordCheck = $("#suaCheck");
        stratfordCheck.click(function(){
            if($(this).is(":checked")){
                $("#stratford").val(1).change();
            } else {
                $("#stratford").val(0).change();
            }
        });
        
        var londonCheck = $("#londonCheck");
        londonCheck.click(function(){
            if($(this).is(":checked")){
                $("#london").val(1).change();
            } else {
                $("#london").val(0).change();
            }
        });
        
        var onTourCheck = $("#onTourCheck");
        onTourCheck.click(function(){
            if($(this).is(":checked")){
                $("#ontour").val(1).change();
            } else {
                $("#ontour").val(0).change();
            }
        });
        
        var eveningCheck = $("#eveningCheck");
        eveningCheck.click(function(){
            if($(this).is(":checked")){
                $("#evening").val(1).change();
            } else {
                $("#evening").val(0).change();
            }
        });
        
        var matineeCheck = $("#matineeCheck");
        matineeCheck.click(function(){
            if($(this).is(":checked")){
                $("#matinee").val(1).change();
            } else {
                $("#matinee").val(0).change();
            }
        });
        
    }
    
});

// GRID ITEMS CLICK FUNCTION

function clickTheGrid(thingy) {
    
    $(thingy).clone().appendTo(gridItemOpenInner);
    gridItemOpenInner.children(".event-grid-item").removeClass("event").addClass("in-focus").removeAttr("onclick");
    var lessButton = gridItemOpenInner.find(".less-button");
    gridItemOpenInner.find(".more-button").toggleClass("open");
    gridItemOpenHolder.toggleClass("opened");
    theBody.toggleClass("noscroll");

    lessButton.toggleClass("closed").click(function(e){
        e.preventDefault();
        gridItemOpenInner.empty();
        gridItemOpenHolder.toggleClass("opened");
        theBody.toggleClass("noscroll");
    });
}

// large screen carousel movement
    
function startCaro() {
    
    var caroLength = carouselItems.length;
    
    if (caroLength > 0){
        
        if (window.innerWidth >= mediumScreen){
            if (caroInverval){
                clearInterval(caroInverval);
                caroInverval = setInterval(function() {
                    caroMovement(caroLength)
                }, 6000);
            } else {
                caroInverval = setInterval(function() {
                    caroMovement(caroLength)
                }, 6000);
            }
        } else if ((window.innerWidth < mediumScreen) && caroInverval){
            clearInterval(caroInverval);
            $(carouselItems[0]).css("margin-left", 0);
        }
    }
}

function caroMovement(caroLen) {

    var theMargin = carouselItems[0].style.marginLeft;
    var pixelMargin = parseInt($(carouselItems[0]).css("margin-left")) * -1;
    var theWidth = carouselItems[0].offsetWidth;

    if (pixelMargin < theWidth * (caroLen - 1)) {

        $(carouselItems[0]).animate({
            marginLeft: '-=100%'
        }, 700);

    } else if (pixelMargin == theWidth * (caroLen - 1)) {
        $(carouselItems[0]).clone().appendTo(caroWrapper);
        $(".carousel-item:last-child").css("margin-left", 0);

        $(carouselItems[0]).animate({
            marginLeft: '-=100%'
        }, 700, function() {
            $(carouselItems[0]).css("margin-left", 0);
            $(".carousel-item:last-child").remove();
        });
    }
}

function videoResize() {
    screenWidth = window.innerWidth;
    
    var videoHoldWidth = largeVideoHolder.width();
    var videoHoldHeight = Math.round((videoHoldWidth / 16) * 9);
    largeIframe.height(videoHoldHeight);
    
    videoHoldWidth = smlVideoHolder.width();
    videoHoldHeight = Math.round((videoHoldWidth / 16) * 9);
    smlVideoHolder.height(videoHoldHeight);
    
    startCaro();
}

window.onresize = videoResize;